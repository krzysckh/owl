
;; new experimental functional vectors with to-be support for assignment and push to end


(define-library (owl vector)

   (import
      (owl core)
      (owl math integer)
      (owl list)
      (owl list-extra)
      (only (owl syscall) error)
      (owl lazy)
      (owl proof)
      )

   (export
      vector
      vector?
      list->vector
      make-vector
      vector->list
      vector-length
      vector-ref   vref
      vector-set   vset
      vector-snoc
      vector-map
      vector-iter
      vector-iterr
      vector-fold
      vector-foldr
      vector-zip
      vector-append)

   (begin

      ;(define type-vector 6) ;; get via (owl core) when in use

      (define (vector? x)
         (eq? (type x) type-vector))

      (define *pad* "<>") ;; an eq? value

      (define (padding n)
         (if (eq? n 0)
            null
            (cons *pad* (padding (- n 1)))))

      (define (hibit n)
         (if (< n 2)
            1
            (let loop ((n (- n 1)) (b 1))
               (if (eq? n 1)
                  b
                  (loop (>> n 1) (<< b 1))))))

      ;; vcons

      (define (vcons a b)
         (if (eq? a *pad*)
            *pad*
            (cons a b)))

      ;; get

      (define (vref-find root bit pos)
         (if (eq? bit 0)
            root
            (lets
               ((x (fxand bit pos))
                (bit _ (fx>> bit 1)))
               (if (eq? x 0)
                  (vref-find (car root) bit pos)
                  (vref-find (cdr root) bit pos)))))

      (define (vref vec pos)
         (cond
            ((not (eq? (type vec) type-vector))
               (error "vector-ref: not a vector: " vec))
            ((not (eq? (type pos) type-fix+))
               (error "vector-ref: position out of fixnum range: " pos))
            (else
               (lets ((root len hibit vec))
                  (if (< pos len)
                     (vref-find root hibit pos)
                     (error "out of vector: " pos))))))

      ;; set

      ; (fxbits fx pos mask) = ((fx & (mask << pos)) >> pos)

      (define (vref-set root bit pos val)
         (if (eq? bit 0)
            val
            (lets
               ((x (fxand bit pos))
                (bit _ (fx>> bit 1)))
               (if (eq? x 0)
                  (cons (vref-set (car root) bit pos val) (cdr root))
                  (cons (car root) (vref-set (cdr root) bit pos val))))))

      (define (vset vec pos val)
         (cond
            ((not (eq? (type vec) type-vector))
               (error "vector-set: not a vector: " vec))
            ((not (eq? (type pos) type-fix+))
               (error "vector-set: position not a fixnum: " pos))
            (else
               (lets ((root len hibit vec))
                  (if (< pos len)
                     (let ((tree (vref-set root hibit pos val)))
                        (mkt type-vector tree len hibit))
                     (error "vector-set: out of vector: " pos))))))

      ;; snoc

      (define (fits-tree? len hi)
         (>= (- (<< hi 1) 1) (- len 1)))

      ;; mostly a clone of vref-set, but also handles node creation
      (define (vref-ins root bit pos val)
         (cond
            ((eq? bit 0)
               val)
            ((eq? root *pad*)
               (vref-ins (cons *pad* *pad*) bit pos val))
            (else
               (lets
                  ((x (fxand bit pos))
                   (bit _ (fx>> bit 1)))
                  (if (eq? x 0)
                     (cons (vref-ins (car root) bit pos val) (cdr root))
                     (cons (car root) (vref-ins (cdr root) bit pos val)))))))

      (define (vsnoc vec val)
         (if (eq? (type vec) type-vector)
            (lets ((root len hibit vec))
               (if (eq? len 0)
                  (mkt type-vector (cons val *pad*) 1 1)
                  (let ((new-len (+ len 1)))
                     (if (fits-tree? new-len hibit)
                        (mkt type-vector (vref-ins root hibit len val) new-len hibit)
                        (let ((hibit (<< hibit 1)))
                           (mkt type-vector
                              (vref-ins (cons root *pad*) hibit len val)
                              new-len
                              hibit))))))
            (error "vector-snoc: not a vector: " vec)))

      ;; construction

      (define (zip-step lst)
         (cond
            ((null? lst)
               null)
            ((null? (cdr lst))
               ;; not a power of 2
               (error "bug in vector construction, list " lst))
            (else
               (cons
                  (vcons (car lst) (cadr lst))
                  (zip-step (cddr lst))))))

      (define (enzip lst n)
         (cond
            ((null? lst)
               (mkt type-vector lst 0 1))
            ((null? (cdr lst))
               (mkt type-vector (car lst) n (hibit n)))
            (else
               (enzip (zip-step lst) n))))

      (define (next-pow n)
         (let loop ((p 2))
            (if (>= p n)
               p
               (loop (<< p 1)))))

      (define (vec lst)
         (let ((len (length lst)))
            (enzip
               (append lst (padding (- (next-pow len) len)))
               len)))

      (define empty-vector
         (vec '()))

      ;; vector mapping

      (define (vmap node bit fn)
         (cond
            ((eq? node *pad*)
               node)
            ((eq? bit 0)
               (fn node))
            (else
               (let ((bit (>> bit 1)))
                  (vcons
                     (vmap (car node) bit fn)
                     (vmap (cdr node) bit fn))))))

      (define (vector-map fn vec)
         (if (eq? (type vec) type-vector)
            (lets ((root len hibit vec))
               (mkt type-vector
                  (vmap root hibit fn)
                  len hibit))
            (error "vector-map: not a vector: " vec)))

      ;; length

      (define (vector-length vec)
         (if (eq? (type vec) type-vector)
            (ref vec 2)
            (error "vector-length: not a vector: " vec)))

      ;; iterator, left to right

      (define (viter-node node bit tail)
         (cond
            ((eq? node *pad*)
               tail)
            ((eq? bit 0)
               (cons node tail))
            (else
               (lets ((bit _ (fx>> bit 1)))
                        (viter-node (car node) bit
                           (λ () (viter-node (cdr node) bit tail)))))))

      (define (vector-iter vec)
         (if (eq? (type vec) type-vector)
            (lets ((root len hibit vec))
               (viter-node root hibit null))
            (error "vector-iter: not a vector: " vec)))

      ;; iterator, right to left

      (define (viterr-node node bit tail)
         (cond
            ((eq? node *pad*)
               tail)
            ((eq? bit 0)
               (cons node tail))
            (else
               (lets ((bit _ (fx>> bit 1)))
                  (viterr-node (cdr node) bit
                     (λ () (viterr-node (car node) bit tail)))))))

      (define (vector-iterr vec)
         (if (eq? (type vec) type-vector)
            (lets ((root len hibit vec))
               (viterr-node root hibit null))
            (error "vector-iterr: not a vector: " vec)))

      ;; folds via iterator for now

      (define (vector-fold o s v)
         (lfold o s (vector-iter v)))

      (define (vector-foldr o s v)
         (lfoldr o s (vector-iterr v)))

      (define (vector-append v1 v2)
         (vector-fold vsnoc v1 v2))

      (define (vector-zip op v1 v2)
         (lfold vsnoc empty-vector
            (lzip op
               (vector-iter v1)
               (vector-iter v2))))

      (define list->vector vec)

      (define (vector->list vec)
         (force-ll (vector-iter vec)))

      (define vector-ref vref)

      (define vector-set vset)

      (define vector-snoc vsnoc)

      (define (vector . args)
         (list->vector args))

      (define (make-vector n . elem?)
         (list->vector
            (make-list n
               (if (null? elem?) #f (car elem?)))))

      (example

         (vector? (vector 1 2 3)) = #t
         (vector? (vector)) = #t
         (vector? (list 1 2 3)) = #f

         (vector-ref (vector 1 2 3) 0) = 1
         (vector-ref (vector 1 2 3) 1) = 2
         (vector-ref (vector 1 2 3) 2) = 3
         (vector-ref (make-vector 100 42) 99) = 42

         (vector-snoc (vector) 42) = (vector 42)
         (vector-snoc (vector 42) 43) = (vector 42 43)
         (vector-snoc (vector 42 43) 44) = (vector 42 43 44)

         (vector-append (vector 1 2 3) (vector 4 5 6)) = (vector 1 2 3 4 5 6)

         (vector-zip + (vector 1 2 3) (vector 10 20 30)) = (vector 11 22 33)

         (vector->list (list->vector '())) = '()
         (vector->list (list->vector '(a))) = '(a)
         (vector->list (list->vector '(a b))) = '(a b)
         (vector->list (list->vector (iota 0 1 100))) = (iota 0 1 100)

         (vector-map (λ (x) (+ x 10)) (vector 1 2 3)) = (vector 11 12 13)

         (vector-fold  - 0 (vector 1 2 3 4)) = (fold  - 0 (list 1 2 3 4))
         (vector-foldr - 0 (vector 1 2 3 4)) = (foldr - 0 (list 1 2 3 4))

         (vset (vector 1 2 3) 0 42) = (vector 42 2 3)
         (vset (vector 1 2 3) 1 42) = (vector 1 42 3)
         (vset (vector 1 2 3) 2 42) = (vector 1 2 42)

      )

))

;(import (owl vector-fun))
;
;(define len 1000000)
;(define l (iota 0 1 len))
;(define v (list->vector l))
;
;(print "LIST FOLD + " len)
;,time (fold + 0 l)
;(print "VECTOR FOLD + " len ", fn 200ms , pairs 100ms")
;,time (vector-fold + 0 v)
;
;;(print "LIST FOLD lref " len)
;;,time (fold (lambda (last p) (lref l p)) 0 l)
;(print "VECTOR FOLD vref " len ", 690ms fn -> 1.35s (pairs)")
;,time (fold (lambda (last p) (vref v p)) 0 l)
;
;;(print "LIST FOLD lset " len)
;;,time (fold (lambda (last p) (lset l p p)) 0 l)
;(print "VECTOR FOLD vset " len ", 1.30s fn, 1.85s")
;,time (fold (lambda (last p) (vector-set v p p)) 0 l)
;
;(import (owl fasl))
;
;(print "list size: " (fold + 0 (map object-size (objects-below l))))
;(print "vect size: " (fold + 0 (map object-size (objects-below v))))
;
